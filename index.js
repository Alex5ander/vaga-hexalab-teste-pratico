const express = require("express");
const mongoose = require("mongoose");
const app = express();
//var uricomauth = "mongodb://user:pass@localhost:27017/database";
var uri = "mongodb://localhost:27017/database";
mongoose.connect(uri,  { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
	if(err){
		console.log("erro \n");
		console.log(err);
	}
});

var quadro = require("./controllers/quadroController");
var tarefa = require("./controllers/tarefaController");
var subtarefa = require("./controllers/subtarefaController");

app.use(express.static("public"));
app.use(express.json());

app.listen(3000, (req, res) => {
	console.log("Start");
});

app.use("/quadros", quadro);
app.use("/tarefas", tarefa);
app.use("/subtarefas", subtarefa);