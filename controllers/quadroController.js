var express = require("express");
var route = express.Router();
var QuadroModel = require(".././models/Quadro");

route.post("/criar", (req, res) => {
	var titulo = req.body.titulo;
	var quadro = new QuadroModel({titulo:titulo});
	quadro.save((err, result) => {
		if(err){
			return res.send(err);
		}
		return res.json({
			success:"ok"
		});
	});
});

route.put("/atualizar", (req, res) => {
	var id = req.body.id;
	var novotitulo = req.body.novotitulo;
	QuadroModel.updateOne({_id:id}, {titulo:novotitulo}, (err, result) => {
		if(err){
			return res.send(err);
		}
		return res.json(result);
	});
});

route.get("/listar", (req, res) => {
	QuadroModel.find((err, result) => {
		if(err){
			console.log(err);
			return res.send(err);
		}
		return res.json(result);
	});
});

route.delete("/deletar", (req, res) => {
	var id = req.body.id;
	var TarefaModel = require(".././models/Tarefa");
	var SubtarefaModel = require(".././models/Subtarefa");
	QuadroModel.deleteOne({_id:id}, (err, result1) => {
		if(err){
			return res.status(404).send(err);
		}
		TarefaModel.deleteMany({quadro_id:id}, (err, result2) => {
			if(err){
				return res.status(404).send(err);
			}
			SubtarefaModel.deleteMany({quadro_id:id}, (err, result3) => {
				if(err){
					return res.status(404).send(err);
				}
				return res.send(result3);
			});
		});
	});
});

module.exports = route;