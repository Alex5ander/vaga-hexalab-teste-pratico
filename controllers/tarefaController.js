var express = require("express");
var route = express.Router();
var TarefaModel = require(".././models/Tarefa");
var QuadroModel = require(".././models/Quadro");

route.post("/criar", async (req, res) => {
	var titulo = req.body.titulo;
	var quadro_id = req.body.quadro_id;
	var status = req.body.status;
	var tarefa = new TarefaModel({titulo:titulo, status:status, quadro_id:quadro_id});
	QuadroModel.updateOne( {_id:quadro_id}, {$push:{tarefas:tarefa}}, (err, quadro) => {
		
		tarefa.save((err, result) => {
			if(err){
				return res.status(404).send(err);
			}
			return res.json({
				success:true
			});
		});
		
	});
});

route.put("/atualizar", (req, res) => {
	var id = req.body.id;
	var quadro_id = req.body.quadro_id;
	var novotitulo = req.body.novotitulo;
	var status = req.body.status;
	TarefaModel.updateOne({_id:id, quadro_id:quadro_id}, {titulo:novotitulo, status:status}, (err, result) => {
		if(err){
			return res.status(404).send(err);
		}
		return res.json(result);
	});
});

route.post("/listar", (req, res) => {
	var quadro_id = req.body.quadro_id;
	QuadroModel.findOne( {_id:quadro_id} ).populate("tarefas")
	.exec(async (err, result) => {
		if(err){
			return res.status(404).send(err);
		}
		return res.send(result.tarefas);
	});
});

route.delete("/deletar", (req, res) => {
	var id = req.body.id;
	var quadro_id = req.body.quadro_id;
	var SubtarefaModel = require(".././models/Subtarefa");
	TarefaModel.deleteOne({_id:id}, (err, result1) => {
		if(err){
			return res.send(err);
		}
		SubtarefaModel.deleteMany({quadro_id:quadro_id}, (err, result2) => {
			if(err){
				return res.send(err);
			}
			QuadroModel.updateOne({_id:quadro_id}, {$pull:{tarefas:id}}, (err, result) => {
				if(err){
					return res.send(err);
				}
				return res.json(result);
			});
		});
	})
});

module.exports = route;