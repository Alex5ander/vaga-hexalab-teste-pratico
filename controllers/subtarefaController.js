var express = require("express");
var route = express.Router();
var SubtarefaModel = require(".././models/Subtarefa");
var TarefaModel = require(".././models/Tarefa");

route.post("/criar", async (req, res) => {
	var titulo = req.body.titulo;
	var tarefa_id = req.body.tarefa_id;
	var status = req.body.status;
	var quadro_id = req.body.quadro_id;
	var subtarefa = new SubtarefaModel({titulo:titulo, status:status, tarefa_id:tarefa_id, quadro_id:quadro_id});
	
	subtarefa.save((err, result) => {
		if(err){
			return res.status(404).send(err);
		}
		TarefaModel.updateOne( {_id:tarefa_id}, {$push:{subtarefas:subtarefa}}, (err, result) => {
		
			if(err){
				return res.status(404).send(err);
			}
			return res.json({
				success:true
			});
			
		});
	});
	
});

route.put("/atualizar", (req, res) => {
	var id = req.body.id;
	var tarefa_id = req.body.tarefa_id;
	var novotitulo = req.body.novotitulo;
	var status = req.body.status;
	SubtarefaModel.updateOne({_id:id, tarefa_id:tarefa_id}, {titulo:novotitulo, status:status}, (err, result) => {
		if(err){
			return res.status(404).send(err);
		}
		return res.json(result);
	});
});

route.post("/listar", (req, res) => {
	var tarefa_id = req.body.tarefa_id;
	TarefaModel.findOne( {_id:tarefa_id} ).populate("subtarefas")
	.exec(async (err, result) => {
		if(err){
			return res.status(404).send(err);
		}
		return res.send(result.subtarefas);
	});
});

route.delete("/deletar", (req, res) => {
	var id = req.body.id;
	var tarefa_id = req.body.tarefa_id;
	SubtarefaModel.deleteOne({_id:id}, (err, result) => {
		if(err){
			console.log(err);
			return res.send(err);
		}
		TarefaModel.updateOne({_id:tarefa_id}, {$pull:{subtarefas:id}}, (err, result) => {
			return res.json(result);
		});
	})
});

module.exports = route;