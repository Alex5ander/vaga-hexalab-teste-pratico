var mongoose = require("mongoose");
var lista_status = ["desenvolvimento", "concluida", "atraso", "cancelada"];
var SubtarefaSchema = new mongoose.Schema(
	{ 
		titulo:{
			type:String, 
			required:true,
			minLength:5
		}, 
		tarefa_id:{
			type:mongoose.Schema.Types.ObjectId,
			ref:"Tarefa",
			required:true
		},
		quadro_id:{
			type:mongoose.Schema.Types.ObjectId,
			ref:"Quadro",
			required:true
		},
		status:{
			type:String, 
			enum:lista_status,
			lowercase:true,
			required:true,
			default:"desenvolvimento"
		} 
	}
);
//collections tarefas
module.exports = mongoose.model("Subtarefa", SubtarefaSchema);