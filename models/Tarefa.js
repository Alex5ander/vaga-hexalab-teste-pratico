var mongoose = require("mongoose");
var lista_status = ["desenvolvimento", "concluida", "atraso", "cancelada"];
var TarefaSchema = new mongoose.Schema(
	{ 
		titulo:{
			type:String, 
			required:true,
			minLength:5
		}, 
		status:{
			type:String, 
			enum:lista_status,
			required:true,
			lowercase:true,
			default:"desenvolvimento"
		},
		quadro_id:{
			type:mongoose.Schema.Types.ObjectId,
			ref:"Quadro",
			required:true
		},
		subtarefas:[
			{
				type:mongoose.Schema.Types.ObjectId,
				ref:"Subtarefa"
			}
		]
	}
);
//collection tarefas
module.exports = mongoose.model("Tarefa", TarefaSchema);