var mongoose = require("mongoose");
var QuadroSchema = new mongoose.Schema(
	{ 
		titulo:{
			type:String, 
			required:true,
			minLength:5
		}, 
		tarefas:[
			{
				type:mongoose.Schema.Types.ObjectId, 
				ref:"Tarefa"
			}
		]
	}
);
//collection quadros
module.exports = mongoose.model("Quadro", QuadroSchema);